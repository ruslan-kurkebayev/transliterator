/**
  * Latin <-> Cyrillic transliteration
  * QWERTY Latin layout <-> Cyrillic layout
**/

var qwertify = require( './src/qwertifier' ),
		modes = {
			qwerty: 'QWERTY'
		},
		directions = {
			lat_cy: 'lat_cy',
			cy_lat: 'cy_lat'
		};

module.exports = {};

module.exports.modes = modes;

module.exports.translite = function translite( text, mode, direction ){
	// Check direction
	if ( !directions.hasOwnProperty( direction ) ){
		throw new Error( 'Transliterator: direction "' + direction + '" is not supperted! List of supported directions: ' + Object.keys( directions ) );
	}

	switch ( mode ){
		case modes.qwerty:
			return qwertify( text, direction );
		default:
			throw new Error( 'Transliterator: mode "' + mode + '" is not supperted! list of supperted modes: ' + Object.values( modes ) );
	}
};
