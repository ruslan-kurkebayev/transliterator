## 0.0.2 - 24/04/2016
- Support for mode "QWERTY" cyrillic <-> latin

## 0.0.1 - 24/04/2016
- Support for mode "QWERTY" cyrillic -> latin added