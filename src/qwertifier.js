var merge = require('merge');

var QWERTY_MAP = merge( true, false,
      require('./qwerty_maps/lat_cy'),
      require('./qwerty_maps/cy_lat')
    );

// Translates text from Latin locale to Cyrillic locale
// Assume supported direction( locale to locale maps ) is passed
function qwertifer( text, direction ){
  var transletedText = '';

  for ( var i = 0; i < text.length; i ++ ){
    var letter = text[i];
    if ( QWERTY_MAP.hasOwnProperty( [direction + '_map'] ) &&
        QWERTY_MAP[direction + '_map'].hasOwnProperty( letter ) ){
      transletedText += QWERTY_MAP[direction + '_map'][letter];
    } else {
      transletedText += letter;
    }
  }
  return transletedText;
}

module.exports = qwertifer;