## Description
* Latin <-> Cyrillic transliteration
* QWERTY Latin layout <-> Cyrillic layout

### Install
```bash
git clone https://ruslan-kurkebayev@bitbucket.org/ruslan-kurkebayev/transliterator.git
npm install
```
or add a dependency to ```package.json``` of your node project
```json
"transliterator": "git+https://bitbucket.org/ruslan-kurkebayev/transliterator.git"
```

## Usage
```javascript
var transliter = require( 'transliterator' );
console.log( transliter.translite( transliter.modes.qwerty, 'F,fz', 'lat_cy' ) ); // Абая
console.log( transliter.translite( transliter.modes.qwerty, 'Абая', 'cy_lat' ) ); // Абая
```

## Tests
```javascript
npm test
```
