var should = require( 'chai' ).should(), /*exported should*/
		transliter = require( '../index.js' );

describe('Transliterator', function() {
  describe( 'mode - QWERTY', function () {
    it( 'should return correct results for direction "lat_cy"', function () {
      var inputValues = [ 'F,fz',
                          'F,fz Hjps,frbtdf',
                          'F,fz / Hjps,frbtdf',
                          'F,fz - Hjps,frbtdf',
                          'Ctqaekkbyf? ujhjl Fcnfyf',
                          'спедиция' ],
          asExpected = [  'Абая',
                          'Абая Розыбакиева',
                          'Абая . Розыбакиева',
                          'Абая - Розыбакиева',
                          'Сейфуллина, город Астана',
                          'спедиция' ];

			for ( var i=0; i<inputValues.length; i++){
				var translited = transliter.translite( inputValues[i], transliter.modes.qwerty, 'lat_cy' );
				translited.should.be.a( 'string' );
    		translited.should.be.equal( asExpected[i] );
			}
    });

    it( 'should return correct results for direction "cy_lat"', function () {
      var inputValues = [ 'Абая',
                          'Абая Розыбакиева',
                          'Абая . Розыбакиева',
                          'Абая - Розыбакиева',
                          'Сейфуллина, город Астана',
                          'pizza house' ],
          asExpected = [  'F,fz',
                          'F,fz Hjps,frbtdf',
                          'F,fz / Hjps,frbtdf',
                          'F,fz - Hjps,frbtdf',
                          'Ctqaekkbyf? ujhjl Fcnfyf',
                          'pizza house' ];

      for ( var i=0; i<inputValues.length; i++){
        var translited = transliter.translite( inputValues[i], transliter.modes.qwerty, 'cy_lat' );
        translited.should.be.a( 'string' );
        translited.should.be.equal( asExpected[i] );
      }
    });
  });
});